/*
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * API version: 0.8.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package goesi

// 200 ok object
type GetFwWars200Ok struct {

	// The faction ID of the enemy faction.
	AgainstId int32 `json:"against_id"`

	// faction_id integer
	FactionId int32 `json:"faction_id"`
}
