/*
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * API version: 0.8.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package goesi

// 200 ok object
type GetFwLeaderboardsOk struct {

	Kills *GetFwLeaderboardsKills `json:"kills"`

	VictoryPoints *GetFwLeaderboardsVictoryPoints `json:"victory_points"`
}
