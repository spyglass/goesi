/*
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * API version: 0.8.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package goesi

// hangar object
type GetCorporationsCorporationIdDivisionsHangar struct {

	// division integer
	Division int32 `json:"division,omitempty"`

	// name string
	Name string `json:"name,omitempty"`
}
