/*
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * API version: 0.8.0
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package goesi

// 422 unprocessable entity object
type PostFleetsFleetIdMembersUnprocessableEntity struct {

	// error message
	Error_ string `json:"error,omitempty"`
}
